

class ClearEdittext():
    def __init__(self, driver):
        self.driver = driver

    def clean_text(self, text):
        '''清空文本框方法的封装'''
        self.driver.keyevent(123)    #123代表光标移动到末尾键
        for i in range(0, len(text)):
            self.driver.keyevent(67)       #67退格键

    def find_ele(self, id):
        '''获取到要删除的文本框内容'''
        self.find_eles = self.driver.find_element_by_id(id)
        self.find_eles.click()
        return self.find_eles.get_attribute('text')

    def Delete(self):
        '''删除文本框内容'''
        get_text = self.find_ele(id)
        self.find_eles.clean_text(get_text)

    def check_Delete(self):
        '''检查文本框是否删除成功'''
        get_text = self.find_ele(id)
        if get_text == "":
            print("文本框删除成功")
        else:
            print("文本框删除失败")

    def editClear(driver, el):
        '''清除EditText文本框里面的文字'''
        el.click()
        # 123将光标移动到末尾
        driver.keyevent(123)
        textLength = len(str(el.text))
        for i in range(0, textLength):
            # 67退格键
            driver.keyevent(67)
