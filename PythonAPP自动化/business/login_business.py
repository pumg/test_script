from handle.login_handle import LoginHandle

class LoginBusiness(object):

    def __init__(self,driver):
        self.login_h = LoginHandle(driver)

    # 进行总的操作进行封装
    def login_base(self,username,password):
        self.login_h.click_bootupn_tvskip()
        self.login_h.click_bootupn_startbutton()
        self.login_h.send_login_username(username)
        self.login_h.send_login_password(password)
        self.login_h.click_rememberme()
        self.login_h.click_agree_checkbox()
        self.login_h.click_login_button()

    # 判断预期结果和实际结果进行返回
    def login_function(self,username,password,asserText):
        self.login_base(username,password)
        if self.login_h.get_toats_text() == asserText:
            print('提示信息正确！预期值与实际值一致:')
            print('预期值：' + asserText)
            print('实际值: ' + self.login_h.get_toats_text())
            return True
        else:
            print('提示信息错误！预期值与实际值不符：')
            print('预期值：' + asserText)
            print('实际值: ' + self.login_h.get_toats_text())
            return False

