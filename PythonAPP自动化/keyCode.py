import os

class InputMethod:

    # 查看当前设备所有的输入法
    def allIME(self):
        os.system("adb shell ime list -s")

    # 查看当前输入法
    def currentIME(self):
        os.system("adb shell settings get secure default_input_method")

    # 切换搜狗输入法为当前输入法
    def enablSougouIME(self):
        os.system("adb shell ime set com.sohu.inputmethod.sogou/.SogouIME")

    # 切换appium输入法为当前输入法
    def enableAppiumcodeIME(self):
        os.system("adb shell ime set io.appium.settings/.UnicodeIME")

# driver直接切换输入法方式
# 切换搜狗输入法
# driver.activate_ime_engine('com.sohu.inputmethod.sogou/.SogouIME')
# 切回appium输入法
# driver.activate_ime_engine('io.appium.settings/.UnicodeIME')