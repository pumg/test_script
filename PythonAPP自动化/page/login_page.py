from common.get_by_local import GetByLocal
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import driverclass
import time
class LoginPage(object):

    def __init__(self,driver):
        self.driver = driver
        self.gl = GetByLocal(driver)

    # 获取开机跳过按钮元素信息
    def get_tvskip_element(self):
        return self.gl.get_element("tvskip", "OneBootupElement")

    # 开始体验按钮元素信息
    def get_startbutton_element(self):
        return self.gl.get_element("Start_button", "OneBootupElement")

    # 获取手机号输入框元素信息
    def get_login_username_element(self):
        return self.gl.get_element("login_username", "LoginElement")

    #  获取密码输入框元素信息
    def get_login_password_element(self):
        return self.gl.get_element("login_password", "LoginElement")

    # 获取记住我勾选按钮元素信息
    def get_login_rememberme_element(self):
        return self.gl.get_element("remember_me", "LoginElement")

    # 获取隐私协议勾选按钮元素信息
    def get_login_agree_checkbox_element(self):
        return self.gl.get_element("agree_checkbox", "LoginElement")

    # 获取登录按钮元素信息
    def get_login_button_element(self):
        return self.gl.get_element("login_button", "LoginElement")

    # 获取忘记密码按钮元素信息
    def get_forget_password_element(self):
        return self.gl.get_element("Forget_password", "LoginElement")

    # 获取创建新用户按钮元素信息
    def get_create_newuser_element(self):
        return self.gl.get_element("Create_newuser", "LoginElement")

    # 获取微信登录按钮元素信息
    def get_wechat_login_element(self):
        return self.gl.get_element("WeChat_login", "LoginElement")

    # 获取服务协议按钮元素信息
    def get_ctmm_Serviceagreement_element(self):
        return self.gl.get_element("XXX_Serviceagreement", "LoginElement")

    # 获取隐私政策按钮元素信息
    def get_privacy_policy(self):
        return self.gl.get_element("Privacy_policy", "LoginElement")

    # 获取提示信息元素信息
    # def get_login_toats_text(self):
        # return self.gl.get_element("toats", "LoginElement")

# if __name__ == "__main__":
#     driver = driverclass.get_android_myvivo()
#     l = LoginPage(driver)
#     l.get_tvskip_element().click()
#     driver.implicitly_wait(2)
#     l.get_startbutton_element().click()
#     driver.implicitly_wait(5)
#     l.get_login_username_element().send_keys("15670963078")
#     driver.implicitly_wait(2)
#     l.get_login_password_element().send_keys("123456")
#     driver.implicitly_wait(2)
#     l.get_login_rememberme_element().click()
#     driver.implicitly_wait(2)
#     l.get_login_agree_checkbox_element().click()
#     driver.implicitly_wait(2)
#     l.get_login_button_element().click()
#     driver.implicitly_wait(2)
#     text = "登录成功"
#     """这儿对于一闪而过的，就经常在第二次去找元素，想拿来某属性做断言，提示 找不到元素"""
#     duanyuan = True
#     try:
#         WebDriverWait(driver, 10, 0.05).until(EC.presence_of_element_located((By.XPATH, '//*[contains(@text, "%s")]' % text[1:3])))
#         print('可以找到这个toast')
#         a = driver.find_element_by_xpath('//*[contains(@text, "%s")]' % text[1:3]).get_attribute('text')
#         print(a)
#
#     except:
#         print('找不到这个toast')
#         duanyuan = False






