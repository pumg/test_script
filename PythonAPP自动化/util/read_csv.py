#coding=utf-8

import csv
def getCsvData(filepath):
    # 读取CSV文件
    value_rows = []
    with open(filepath, encoding='UTF-8') as f:
        f_csv = csv.reader(f)
        next(f_csv)
        for r in f_csv:
            value_rows.append(r)
    return value_rows