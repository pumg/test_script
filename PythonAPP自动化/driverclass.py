
from appium import webdriver

# 默认服务器地址
server_address = 'http://127.0.0.1:4723/wd/hub'
def get_android_nox():
    '''连接手机app初始化的一些信息'''
    desc={}
    desc['deviceName']='127.0.0.1:62001'    # 手机设备名称，adb devices
    desc['platformVersion']='5.1.1'         # 手机版本，在手机中：设置--关于手机
    desc['platformName']='Android'          # 手机类型，ios或android
    #输入命令，获取app信息：aapt dump badging C:\Users\83473\Desktop\mobileqq_android.apk
    desc['appPackage']='com.XXX.XXX'        #包名
    desc['appActivity']='com.ctzn.ctmm.ui.activity.WelcomeActivity'     #启动入口
    desc["unicodeKeyboard"] = "True"        # 使用unicode编码方式发送字符串
    desc["resetKeyboard"] = "True"          # 隐藏键盘
    desc["noReset"] = "True"                # 不初始化手机app信息（类似不清除缓存）
    desc["automationName"] = "Uiautomator2"
    driver = webdriver.Remote(server_address, desc)
    driver.implicitly_wait(5)
    return driver


def get_android_huawei():
    desired_caps={
            "deviceName": "SCL-CL00",
            "platformVersion": "5.1.1",
            "platformName": "Android",
            "appPackage": "com.ctzn.ctmm",
            "appActivity": "com.ctzn.ctmm.ui.activity.WelcomeActivity",
            "unicodeKeyboard": "True",
            "resetKeyboard": "True",
            "noReset": "True",
            "automationName": "Uiautomator2"
            }
    driver=webdriver.Remote(server_address,desired_caps)
    driver.implicitly_wait(5)
    return driver

def get_android_myvivo():
    desired_caps={
        "deviceName": "4c8290a0",
        "platformVersion": "8.1.0",
        "platformName": "Android",
        "appPackage": "com.ctzn.ctmm",
        "appActivity": "com.ctzn.ctmm.ui.activity.WelcomeActivity",
        "unicodeKeyboard": "True",
        "resetKeyboard": "True",
        # "noReset": "True",
        "automationName": "Uiautomator2"
    }
    deiver=webdriver.Remote(server_address,desired_caps)
    deiver.implicitly_wait(50)
    return deiver


