from page.login_page import LoginPage
import driverclass

class LoginHandle(object):

    def __init__(self,driver):
        self.driver = driver
        self.login_p = LoginPage(self.driver)

    # 点击跳过开机按钮
    def click_bootupn_tvskip(self,):
        self.login_p.get_tvskip_element().click()

    # 点击点击开始体验按钮
    def click_bootupn_startbutton(self):
        self.login_p.get_startbutton_element().click()

    # 输入登录的手机号
    def send_login_username(self,user):
        self.login_p.get_login_username_element().send_keys(user)

    # 输入登录的密码
    def send_login_password(self,password):
        self.login_p.get_login_password_element().send_keys(password)

    # 点击登录按钮
    def click_login_button(self):
        self.login_p.get_login_button_element().click()

    # 勾选协议
    def click_agree_checkbox(self):
        self.login_p.get_login_agree_checkbox_element().click()

    # 勾选记住我
    def click_rememberme(self):
        self.login_p.get_login_rememberme_element().click()

    # 点击忘记密码按钮
    def click_forget_password(self):
        self.login_p.get_forget_password_element().click()

    # 点击创建新用户按钮
    def click_create_newuser(self):
        self.login_p.get_create_newuser_element().click()

    # 点击微信登录按钮
    def click_wechat_login(self):
        self.login_p.get_wechat_login_element().click()

    #点击服务协议
    def click_ctmm_serviceagreement(self):
        self.login_p.get_ctmm_Serviceagreement_element().click()

    # 点击隐私政策
    def click_privacy_policy(self):
        self.login_p.get_privacy_policy().click()

    # 获取Toats提示信息
    def get_toats_text(self, text):
        if text == None:
            return "没有获取到错误信息!"
        else:
            return text.text
