# 测试脚本
## Python
* [x] [接口自动化（request + Excel）](https://gitee.com/pumg/test_script/tree/master/Python接口自动化)
* [x] [Web自动化（PO模式 + selenium + unittest + ddt + HTMLrunner）](https://gitee.com/pumg/test_script/tree/master/PythonWeb自动化)
* [x] [App自动化（PO模式 + appium + unittest + ddt + HTMLrunner）](https://gitee.com/pumg/test_script/tree/master/PythonAPP自动化)
* [x] [测试平台开发（Vue + Element UI + Django + Django RestFramework + MySQL + HttpRunner）](https://gitee.com/pumg/test_script/tree/master/Python接口测试平台)


## Java
* [ ] 接口自动化
* [ ] Web自动化
* [ ] App自动化
* [ ] 测试平台开发

## 前端
* [x] [前端开发（Vue + Element UI）](https://gitee.com/pumg/test_script/tree/master/Python接口测试平台/VuePlatform)

