"""TestPlatform URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework.documentation import include_docs_urls
# from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title='测开项目接口文档',    # 必传
        default_version='v1',    # 必传
        description='这是刘翔的测开平台项目接口文档',
        terms_of_service='https://csq.world',
        contact=openapi.Contact(email='905364660@qq.com'),
        license=openapi.License(name='BSD License'),
    ),
    public=True
    # permission_classes=(permissions.AllowAny),    # 权限类
)

urlpatterns = [
    # path('api/admin/', admin.site.urls),
    path('api/', include('projects.urls')),
    path('api/user/', include('user.urls')),
    path('api/', include('modulars.urls')),
    path('api/', include('debugtalks.urls')),
    path('api/', include('envs.urls')),
    path('api/', include('reports.urls')),
    path('api/', include('configures.urls')),
    path('api/', include('testcases.urls')),
    path('api/',include('summary.urls')),
    path('api/', include('tasks.urls')),

    # path('api/', include('rest_framework.urls')),
    path('api/docs/', include_docs_urls(title='测试平台项目接口文档',
                                    description='这是刘翔开发的接口测试平台')),
    # path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-ui'),
]
