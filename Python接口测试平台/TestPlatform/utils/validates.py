from rest_framework import serializers

from modulars.models import Modulars
from projects.models import Projects
from envs.models import Envs


def whether_existed_project_id(value):
    """
    检查项目id是否存在
    :param value:
    :return:
    """

    if not isinstance(value, int):
        raise serializers.ValidationError('所选项目有误!')
    elif not Projects.objects.filter(is_delete=False, id=value).exists():
        raise serializers.ValidationError('所选项目不存在!')


def whether_existed_modulars_id(value):
    """
    检查模块id是否存在
    :param value:
    :return:
    """
    if not isinstance(value, int):
        raise serializers.ValidationError('所选模块有误！')
    elif not Modulars.objects.filter(is_delete=False, id=value).exists():
        raise serializers.ValidationError('所选模块不存在！')

def whether_existed_env_id(value):
    """
    检查环境id是否存在
    :param value:
    :return:
    """
    if value != 0:
        if not isinstance(value, int):
            raise serializers.ValidationError("所选环境有误！")
        elif not Envs.objects.filter(is_delete=False, id=value).exists():
            raise serializers.ValidationError("所选环境不存在！")


