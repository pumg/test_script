
def handle_data1(datas):
    """
    将[{'check': 'status_code', 'expected': 200, 'comparator': 'equals'}]
    转化为[{keyName: 'status_code', value:200, comparator: 'equals', valueType: 'string'}]
    :param datas:
    :return:
    """
    result_list = []
    if datas is not None:
        for one_validate_dict in datas:
            key = one_validate_dict.get('check')
            value = one_validate_dict.get('expected')
            comparator = one_validate_dict.get('comparator')
            result_list.append({
                'keyName': key,
                'value': value,
                'comparator': comparator,
                'valueType': handle_param_type(value)
            })
    return result_list


def handle_data2(datas):
    """
    将[{'age': 18}]
    转化为[{key: 'age', value: 18, valueType: 'int'}]
    :param datas: 待转化的参数列表
    :return:
    """
    result_list = []
    if datas is not None:
        for one_var_dict in datas:
            key = list(one_var_dict)[0]
            value = one_var_dict.get(key)
            result_list.append({
                'key': key,
                'value': value,
                'valueType': handle_param_type(value)
            })
    return result_list


def handle_data3(datas):
    """
    将[{'token': 'content.token'}]
    转化为[{keyName: 'token', value: 'content.token'}]
    :param datas:
    :return:
    """
    result_list = []
    if datas is not None:
        for one_dict in datas:
            key = list(one_dict)[0]
            value = one_dict.get(key)
            result_list.append({
                'keyName': key,
                'value': value
            })
    return result_list


def handle_data4(dadas):
    """
    将{'User-Agent': 'Mozilla/5.0 LiuXiang'}
    转化为[{keyName: 'User-Agent', value: 'Mozilla/5.0 LiuXiang'}]
    :param dadas:
    :return:
    """
    result_list = []
    if dadas is not None:
        for key, value in dadas.items():
            result_list.append({
                'keyName': key,
                'value': value
            })
    return result_list


def handle_data5(datas):
    """
    将['${setup_hook_prepare_kwargs($request)}', '${setup_hool_httpntlmauth($reqest)}']
    转化为[{keyName: '${setup_hook_prepare_kwargs($request)}'}, {keyName: ${setup_hool_httpntlmauth($reqest)}}]
    :param datas:
    :return:
    """
    result_list = []
    if datas is not None:
        for item in datas:
            result_list.append({
                'keyName': item
            })
    return result_list

def handle_data6(datas):
    """
    将{'username': 'keyou', 'age': 18, 'gender': True}
    转化为[{keyName: 'username', value: 'liuxiang', valueType: 'string'}, {keyName: 'age', value: 18, valueType: 'int'}……]
    :param datas:
    :return:
    """
    result_list = []
    if datas is not None:
        for key, value in datas.items():
            result_list.append({
                'keyName': key,
                'value': value,
                'valueType': handle_param_type(value),
            })
    return result_list


def handle_param_type(value):
    """
    处理参数类型
    :param value: 数据的类型名
    :return:
    """
    if isinstance(value, int):
        param_type = 'int'
    elif isinstance(value, float):
        param_type = 'float'
    elif isinstance(value, bool):
        param_type = 'boolean'
    else:
        param_type = 'string'
    return param_type


















