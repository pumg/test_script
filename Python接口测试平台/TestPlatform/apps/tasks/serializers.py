from rest_framework import serializers

from projects.models import Projects
from tasks.models import Tasks
from utils import validates


class TasksModelSerializer(serializers.ModelSerializer):
    project = serializers.StringRelatedField(help_text='项目名称')
    project_id = serializers.PrimaryKeyRelatedField(queryset=Projects.objects.all(),
                                                    help_text='项目ID')
    class Meta:
        model = Tasks
        fields = ('id', 'name', 'tester', 'project', 'project_id', 'env_id', 'caseset', 'status', 'email', 'strategy', 'desc', 'create_time')
        extra_kwargs = {
            'create_time': {
                'read_only': True
            }
        }

    def create(self, validated_data):
        project = validated_data.pop('project_id')
        validated_data['project'] = project
        tasks = Tasks.objects.create(**validated_data)
        return tasks

    def update(self, instance, validated_data):
        if 'project_id' in validated_data:
            project = validated_data.pop('project_id')
            validated_data['project'] = project
        return super().update(instance, validated_data)


class TasksRunSerializer(serializers.ModelSerializer):
    """
    通过项目运行测试用例序列化器
    """
    env_id = serializers.IntegerField(write_only=True,
                                      help_text='环境变量ID',
                                      validators=[validates.whether_existed_env_id])

    class Meta:
        model = Tasks
        fields = ('id', 'env_id')


