from rest_framework import routers

from .views import TasksViewSet

router = routers.DefaultRouter()
router.register('tasks', TasksViewSet)
urlpatterns = [
]
urlpatterns += router.urls