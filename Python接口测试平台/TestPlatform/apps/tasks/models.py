from django.db import models

# Create your models here.
from utils.base_models import BaseModel


class Tasks(BaseModel):
    id = models.AutoField(verbose_name='id主键', primary_key=True, help_text='id主键')
    name = models.CharField('任务名称', max_length=50, help_text='任务名称')
    project = models.ForeignKey('projects.Projects', on_delete=models.CASCADE,
                                related_name='tasks', help_text='所属项目')
    env_id = models.CharField('环境ID', max_length=50, help_text='环境ID')
    caseset = models.CharField('环境ID', max_length=500, help_text='环境ID', default='[]')
    status = models.BooleanField(default=False, verbose_name='任务状态', help_text='任务状态')
    strategy = models.CharField('执行策略', max_length=200, help_text='执行策略', default="* * * * *")
    desc = models.CharField('简要描述', max_length=200, help_text='简要描述', blank=True, default="", null=True)
    email = models.CharField('邮箱地址', max_length=50, help_text='邮箱地址', blank=True, default="905364660@qq.com", null=True)
    tester = models.CharField('测试人员', max_length=50, help_text='测试人员')

    class Meta:
        db_table = 'tb_tasks'
        verbose_name = '定时任务'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name