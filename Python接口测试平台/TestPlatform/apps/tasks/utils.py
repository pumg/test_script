from tasks.models import Tasks
import json




def read_cron():
    cron = Tasks.objects.filter(is_delete=False, status=True)
    cron_list = []
    for c in cron:
        cron_list.append(
            {
                "cron": c.strategy,
                "func": "apps/tasks/views.run_cron",
                "args": c.id
            }
        )
    with open("utils/cron.json", "w") as fp:
        fp.write(json.dumps(cron_list))


