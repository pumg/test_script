from projects import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('projects', views.ProjectsViewSet)

urlpatterns = []
urlpatterns += router.urls

