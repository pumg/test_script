from django.contrib import admin

# Register your models here.
from projects.models import Projects

class ProjectsAdmin(admin.ModelAdmin):
    """定制后台站点类"""
    # 指定修改(新增)中需要的字段
    fields = ('name', 'leader', 'tester', 'programer', 'pulish_app')
    # 新增要列出的字段
    list_display = ['id', 'name', 'leader', 'tester']

admin.site.register(Projects, ProjectsAdmin)