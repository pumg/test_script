import re
from django.db.models import Count

from modulars.models import Modulars
from configures.models import Configures


def get_count_by_project(datas):
    datas_list = []
    for item in datas:
        mtch = re.search(r'(.*)T(.*):.*?', item['create_time'])
        item['create_time'] = mtch.group(1) + ' ' + mtch.group(2)
        project_id = item['id']

        # 计算模块总数
        modulars_testcase_objs = Modulars.objects.values('id').annotate(testcases=Count('testcases')).\
            filter(project_id=project_id, is_delete=False)
        modulars_count = modulars_testcase_objs.count()

        # 计算用例总数
        testcases_count = 0
        for one_dict in modulars_testcase_objs:
            testcases_count += one_dict['testcases']

        # 计算配置总数
        project_configures_objs = Configures.objects.values('id').filter(project_id=project_id, is_delete=False)
        configures_count = project_configures_objs.count()

        item['modulars'] = modulars_count
        item['testcases'] = testcases_count
        item['configures'] = configures_count

        datas_list.append(item)

    return datas_list