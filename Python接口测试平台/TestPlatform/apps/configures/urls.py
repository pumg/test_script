from rest_framework import routers

from .views import ConfiguresViewSet

router = routers.DefaultRouter()
router.register('configures', ConfiguresViewSet)

urlpatterns = [

]

urlpatterns += router.urls