from datetime import datetime
import os

from django.conf import settings
from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions, status
from rest_framework.decorators import action
from rest_framework.response import Response

from envs.models import Envs
from utils import common
from .models import Modulars
from modulars import serializers
from .utils import get_count_by_project
from testcases.models import Testcases
from configures.models import Configures


class ModularsViewSet(ModelViewSet):
    """
    list:
    返回模块(多个)列表数据

    create:
    创建模块

    retrieve:
    返回模块(单个)详情数据

    update:
    更新(全部)模块

    partial_update:
    更新(部分)模块

    destroy:
    删除模块

    testcases:
    获取某个模块下的所有用例

    configures:
    获取某个模块下的所有配置

    run:
    运行模块下的所有用例
    """
    queryset = Modulars.objects.filter(is_delete=False)
    serializer_class = serializers.ModularsSerializer
    permission_classes = [permissions.IsAuthenticated,]
    ordering_fields = ('id', 'name')

    def perform_destroy(self, instance):
        instance.is_delete = True
        instance.save()     # 逻辑删除

    @action(methods=['get'], detail=True)
    def testcases(self, request, pk=None):
        testcase_objs = Testcases.objects.filter(modular_id=pk, is_delete=False)
        one_list = []
        for obj in testcase_objs:
            one_list.append({
                'id': obj.id,
                'name': obj.name
            })
        return Response(data=one_list)

    @action(methods=['get'], detail=True, url_path='configs')
    def configures(self, request, pk=None):
        configures_objs = Configures.objects.filter(interface_id=pk, is_delete=False)
        one_list = []
        for obj in configures_objs:
            one_list.append({
                'id': obj.id,
                'name': obj.name
            })
        return Response(data=one_list)

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        response.data['results'] = get_count_by_project(response.data['results'])
        return response


    @action(methods=['post'], detail=True)
    def run(self, request, pk=None):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        datas = serializer.validated_data
        env_id = datas.get('env_id')    # 获取环境变量env_id

        # 创建测试用例所在目录名
        testcase_dir_path = os.path.join(settings.SUITES_DIR,
                                         datetime.strftime(datetime.now(), '%Y%m%d%H%M%S%f'))
        if not os.path.exists(testcase_dir_path):
            os.mkdir(testcase_dir_path)

        env = Envs.objects.filter(id=env_id, is_delete=False).first()
        testcase_objs = Testcases.objects.filter(is_delete=False, modular=instance)

        if not testcase_objs.exists():
            data_dict = {
                'detail': '此模块下无用例，无法运行！'
            }
            return Response(data_dict, status=status.HTTP_400_BAD_REQUEST)

        for one_obj in testcase_objs:
            common.generate_testcase_files(one_obj, env, testcase_dir_path)

        # 运行用例
        return common.run_testcase(instance, testcase_dir_path)

    def get_serializer_class(self):
        """
        不同的action选择不同的序列化器
        :return:
        """
        return serializers.ModularsRunSerializer if self.action == 'run' else serializers.ModularsSerializer
