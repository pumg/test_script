from rest_framework import routers
from modulars.views import ModularsViewSet

router = routers.DefaultRouter()
router.register('modulars', ModularsViewSet)
urlpatterns = []
urlpatterns += router.urls