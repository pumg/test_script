from django.apps import AppConfig


class ModularsConfig(AppConfig):
    name = 'modulars'
