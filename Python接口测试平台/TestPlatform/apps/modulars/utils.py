import re
from django.db.models import Count

from configures.models import Configures
from modulars.models import Modulars

def get_count_by_project(datas):
    datas_list = []
    for item in datas:
        # 将create_time进行格式化
        mtch = re.search('(.*)T(.*):.*?', item['create_time'])
        item['create_time'] = mtch.group(1) + ' ' + mtch.group(2)
        project_id = item['id']
        #计算用例数
        modulars_testcase_objs = Modulars.objects.values('id').annotate(testcases=Count('testcases')).\
            filter(project_id=project_id, is_delete=False)
        testcases_count = 0
        for one_dict in modulars_testcase_objs:
            testcases_count += one_dict['testcases']
        item['testcases'] = testcases_count
        datas_list.append(item)
    return datas_list