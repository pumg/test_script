from rest_framework import routers

from .views import TestcasesViewSet

router = routers.DefaultRouter()
router.register('testcases', TestcasesViewSet)
urlpatterns = [
]
urlpatterns += router.urls