import re

from .models import Envs

def handle_env(datas):
    datas_list = []
    for item in datas:
        # 将create_time进行格式化
        mtch = re.search(r'(.*)T(.*):.*?', item['create_time'])
        item['create_time'] = mtch.group(1) + ' ' + mtch.group(2)
        datas_list.append(item)
    return datas_list