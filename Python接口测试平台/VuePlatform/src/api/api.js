import axios from 'axios';

// let host = 'http://122.51.167.166/api';

let host = 'http://127.0.0.1:9090/api';

// 登录
export const login = params => { return axios.post(`${host}/user/login/`, params)};

// 注册
export const register = params => { return axios.post(`${host}/user/register/`, params)};

// 获取总数据
export const summary = () => { return axios.get(`${host}/summary/`)};


// ------------------------获取项目列表信息------------------------
export const projects_list = params => { return axios.get(`${host}/projects/`, {params: params})};

// 删除某个项目
export const delete_project = id => { return axios.delete(`${host}/projects/` + id + `/`)};

// 获取单个项目
export const read_project = id => { return axios.get(`${host}/projects/` + id + `/`)};

// 修改某个项目
export const edit_project = (id, params) => { return axios.put(`${host}/projects/` + id + `/`, params)};

// 获取项目name和id
export const projects_names = () => { return axios.get(`${host}/projects/names/`)};

// 新增项目
export const add_projects = params => { return axios.post(`${host}/projects/`, params)};

// 运行项目下的所有用例
export const run_project = (id, env_id) => { return axios.post(`${host}/projects/` + id + `/run/`, env_id) }

// ------------------------获取模块列表信息------------------------
export const modulars_list = params => { return axios.get(`${host}/modulars/`, {params: params})};

// 修改某个模块
export const edit_modulars = (id, params) => { return axios.put(`${host}/modulars/`+ id + `/`, params)}

// 新增模块
export const add_modulars = params => { return axios.post(`${host}/modulars/`, params)}

// 删除某个模块
export const delete_modular = id => { return axios.delete(`${host}/modulars/` + id + `/`)};

// 获取某项目下的所有模块
export const project_in_modulars = id => { return axios.get(`${host}/projects/` + id + `/modulars/`) }

// 获取某模块下所有的用例
export const modulars_in_case = id => { return axios.get(`${host}/modulars/` + id + `/testcases/`) }

// 运行模块下的所有用例
export const run_modular = (id, env_id) => { return axios.post(`${host}/modulars/` + id + `/run/`, env_id) }

// 获取指定项目下的所有用例
export const pcase_in_project = id => { return axios.get(`${host}/projects/` + id + `/pcase/`) }


// ------------------------获取配置列表信息------------------------
export const configures_list = params => { return axios.get(`${host}/configures/`, {params: params})};

// 获取指定配置信息的详情
export const read_configure = id => { return axios.get(`${host}/configures/` + id + `/`) }

// 修改某个模块
export const edit_configures = (id, params) => { return axios.put(`${host}/configures/`+ id + `/`, params)}

// 新增模块
export const add_configures = params => { return axios.post(`${host}/configures/`, params)}

// 删除某个模块
export const delete_configures = id => { return axios.delete(`${host}/configures/` + id + `/`)};


// ------------------------获取用例列表------------------------
export const cases_list = params => { return axios.get(`${host}/testcases/`, {params: params}) }

// 新增用例
export const add_case = params => { return axios.post(`${host}/testcases/`, params) }

// 获取某个项目下的所有配置
export const config_in_project = id => { return axios.get(`${host}/projects/` + id + `/configures/`) }

// 获取某个用例
export const read_case = id => { return axios.get(`${host}/testcases/` + id + `/`) }

// 修改某个用例
export const edit_case = (id, params) => { return axios.put(`${host}/testcases/` + id + `/`, params)}

// 删除某个用例
export const delete_case = id => { return axios.delete(`${host}/testcases/` + id + `/`)}

// 运行用例
export const run_case = (id, env_id) => { return axios.post(`${host}/testcases/` + id + `/run/`,env_id) }


// ------------------------获取定时任务列表------------------------
export const tasks_list = params => { return axios.get(`${host}/tasks/`, {params: params}) }

// 获取定时任务详情
export const read_tasks = id => { return axios.get(`${host}/tasks/` + id + `/`) }

// 运行定时任务
export const run_tasks = (id, env_id) => { return axios.post(`${host}/tasks/` + id + `/run/`, env_id)}

// 添加定时任务
export const add_tasks = params => { return axios.post(`${host}/tasks/`, params)}

// 修改(全部)某个定时任务
export const edits_tasks = (id, params) => { return axios.put(`${host}/tasks/` + id + `/`, params)}

// 修改(部分)某个定时任务
export const edit_tasks = (id, params) => { return axios.patch(`${host}/tasks/` + id + `/`, params)}

// 删除某个定时任务
export const delete_tasks = id => { return axios.delete(`${host}/tasks/` + id + `/`)}

// 获取项目下的所有模块及用例
export const p_models_and_cases = id => { return axios.get(`${host}/tasks/` + id + `/p_models_and_cases/`)}


// ------------------------获取Debugtalk列表------------------------
export const debugtalk_list = params => { return axios.get(`${host}/debugtalks/`, {params: params}) }

// 获取某个debugtalk详情
export const read_debugtalk = id => { return axios.get(`${host}/debugtalks/` + id + `/`) }

// 修改某个debugtalk
export const edit_debugtalk = (id, params) => { return axios.put(`${host}/debugtalks/` + id + `/`, params)}

// 删除某个debugtalk
export const delete_debugtalk = id => { return axios.delete(`${host}/debugtalks/` + id + `/`)}


// ------------------------获取环境列表------------------------
export const envs_list = params => { return axios.get(`${host}/envs/`, {params: params}) }

//获取所有环境变量的name和id
export const nameAndid_in_envs = () => { return axios.get(`${host}/envs/names/`) }

// 新增环境
export const add_env = params => { return axios.post(`${host}/envs/`, params) }

// 获取某个环境
export const read_env = id => { return axios.get(`${host}/envs/` + id + `/`) }

// 修改某个环境
export const edit_env = (id, params) => { return axios.put(`${host}/envs/` + id + `/`, params)}

// 删除某个环境
export const delete_env = id => { return axios.delete(`${host}/envs/` + id + `/`)}

// ------------------------获取报告列表------------------------
export const reports_list = params => { return axios.get(`${host}/reports/`, {params: params}) }

// 获取某个报告
export const read_reports = id => { return axios.get(`${host}/reports/` + id + `/`) }

// 删除某个报告
export const reports_env = id => { return axios.delete(`${host}/reports/` + id + `/`)}

// 下载报告
export const reports_download = id => { return axios.get(`${host}/reports/` + id + `/download/`) }