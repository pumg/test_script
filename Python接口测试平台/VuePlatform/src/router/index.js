import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/login',
            name: '/login',
            component: () => import(/* webpackChunkName: "login" */ '../components/page/Login.vue'),
            meta: { title: '登录' }
        },
        {
            path: '/',
            redirect: '/dashboard'
        },
        {
            path: '/',
            component: () => import(/* webpackChunkName: "home" */ '../components/common/Home.vue'),
            meta: { title: '自述文件' },
            children: [
                {
                    // 系统主页
                    path: '/dashboard',
                    component: () => import(/* webpackChunkName: "dashboard" */ '../components/page/Dashboard.vue'),
                    meta: { title: '系统首页' }
                },
                {
                    // 项目管理页
                    path: '/projects',
                    component: () => import(/* webpackChunkName: "projects" */ '../components/page/ProjectsList.vue'),
                    meta: { title: '项目列表' }
                },
                {
                    // 模块管理页
                    path: '/modulars',
                    component: () => import(/* webpackChunkName: "modulars" */ '../components/page/ModularsList.vue'),
                    meta: { title: '模块列表' }
                },
                {
                    // 配置列表页
                    path: '/ConfiguresList',
                    component: () => import('../components/page/ConfiguresList.vue'),
                    meta: { title: '配置管理'}
                },
                {
                    // 新增配置页
                    path: '/ConfigureAdd',
                    component: () => import('../components/page/ConfigureAdd.vue'),
                    meta: { title: '新增配置'}
                },
                {
                    // 编辑配置页
                    path: 'ConfigureEdit',
                    name: 'ConfigureEdit',
                    component: () => import('../components/page/ConfigureEdit.vue'),
                    meta: { title: '编辑配置' }
                },
                {
                    // 用例列表页
                    path: '/CaseList',
                    component: () => import(/* webpackChunkName: "cases" */ '../components/page/CaseList.vue'),
                    meta: { title: '用例列表' }
                },
                {
                    // 新增用例页
                    path: '/CaseAdd',
                    component: () => import(/* webpackChunkName: "addcase" */ '../components/page/CaseAdd.vue'),
                    meta: { title: '新增用例' }
                },
                {
                    // 编辑用例页
                    path: '/caseedit',
                    name: 'caseedit',
                    component: () => import(/* webpackChunkName: "addcase" */ '../components/page/CaseEdit.vue'),
                    meta: { title: '编辑用例' }
                },
                {
                    //debugtalk列表页
                    path: '/DebugtalkList',
                    name: '/DebugtalkList',
                    component: () => import('../components/page/DebugtalkList.vue'),
                    meta: { title: 'Debugtalk' }
                },
                {
                    //debugtalk编辑页
                    path: '/DebugtalkEdit',
                    name: '/DebugtalkEdit',
                    component: () => import('../components/page/DebugtalkEdit.vue'),
                    meta: { title: 'Debugtalk编辑' }
                },
                {
                    // 测试页
                    path: '/upload',
                    component: () => import(/* webpackChunkName: "upload" */ '../components/page/Upload.vue'),
                    meta: { title: '文件上传' }
                },
                // 运行环境页
                {
                    path: '/EnvList',
                    component: () => import('../components/page/EnvList.vue'),
                    meta: { title: '环境列表'}
                },
                // 定时任务页
                {
                    path: '/Crontab',
                    component: () => import('../components/page/Crontab.vue'),
                    meta: { title: '定时任务' }
                },
                // 测试报告页
                {
                    path: '/ReportsList',
                    component: () => import('../components/page/ReportsList.vue'),
                    meta: { title: '报告列表'}
                },
                {
                    path: '/404',
                    component: () => import(/* webpackChunkName: "404" */ '../components/page/404.vue'),
                    meta: { title: '404' }
                },
                {
                    path: '/403',
                    component: () => import(/* webpackChunkName: "403" */ '../components/page/403.vue'),
                    meta: { title: '403' }
                },
                {
                    path: '/reportsdetails',
                    name: 'reportsdetails',
                    component: () => import(/* webpackChunkName: "403" */ '../components/page/ReprotsDetails.vue'),
                    meta: { title: '报告详情' }
                },
            ]
        },
        {
            path: '*',
            redirect: '/404'
        }
    ]
});
