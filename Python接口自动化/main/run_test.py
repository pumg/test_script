from base.runmethod import RunMethod
from util.operation_json import OperationJson
from data.get_data import GetData
from util.operation_header import OperationHeader
from data.depend_data import DependdentData


class RunTest:

    def __init__(self):
        self.run_method = RunMethod()
        self.data = GetData()
        self.op_header = OperationHeader()

    def go_on_run(self):
        """ 程序执行 """
        rows_count = self.data.get_case_lines()
        for i in range(1, rows_count):
            # 先获取是否执行字段，然后进行判断
            is_run = self.data.get_is_run(i)
            if is_run:
                # 获取i行的整行数据
                url = self.data.get_request_url(i)
                method = self.data.get_request_method(i)
                request_data = self.data.get_data_for_json(i)
                expect = self.data.get_expect_data(i)
                header = self.data.is_header(i)
                depend_case = self.data.is_depend(i)
                # 如果用例依赖字段不为空
                if depend_case != None:
                    depend_ca = depend_case.split(",")
                    deta = len(depend_ca)
                    for num in range(deta):
                        # 获取依赖得返回数据
                        self.depend_data = DependdentData(depend_ca[num])
                        # 获取的依赖响应数据
                        depend_repsonse_data = self.depend_data.get_data_for_key(i, num)
                        # 获取依赖的key
                        depend_key = self.data.get_depend_filed(i, num)
                        # 更新请求数据
                        request_data[depend_key] = depend_repsonse_data
                        print("=================更新过的数据为：=================")
                        print(request_data)
                # 如果header等于写，就写入将获取到header的值写入到header.json文件中
                if header == "write":
                    res = self.run_method.run_mian(method, url, request_data)
                    # cookie = self.op_header.get_response_cookie(method, url, request_data)
                    token = self.op_header.get_response_token(res)
                    self.op_header.write_headers(token=token)
                # 如果等于yes，就用header.json文件里面的数据
                elif header == "yes":
                    op_json = OperationJson("../dataconfig/header.json")
                    header = op_json.read_data()
                    res = self.run_method.run_mian(method, url, request_data, header)
                # 否则不携带header数据
                else:
                    res = self.run_method.run_mian(method, url, request_data)
                print("响应参数：" + res)
                if expect == "":
                    print("预期结果为空")
                elif expect in res:
                    self.data.write_result(i, "pass")
                else:
                    self.data.write_result(i, "false")


if __name__ == "__main__":
    run = RunTest()
    run.go_on_run()

















