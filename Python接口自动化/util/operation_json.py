import json


class OperationJson:

    def __init__(self, file_path=None):
        if file_path:
            self.file_path = file_path
        else:
            self.file_path = "../dataconfig/request_parameters.json"
        self.data = self.read_data()

    def read_data(self):
        """ 读取json文件数据 """
        with open(self.file_path, "r", encoding="UTF-8") as fp:
            data = json.load(fp)
            return data

    def get_data(self, key):
        """ 根据关键字获取数据 """
        if key is None:
            return None
        return self.data[key]

    def write_data(self, data):
        """ 写json到文件中 """
        with open("../dataconfig/header.json", "w") as fp:
            fp.write(json.dumps(data))
