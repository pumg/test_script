import xlrd
from xlutils.copy import copy


class OperatinonExcel:
    """ 操作excel类 """
    def __init__(self, file_name=None, sheet_id=None):
        """
        定义全局excel的路径和sheet位置
        :param file_name:
        :param sheet_id:
        """
        if file_name:
            self.file_name = file_name
            self.sheet_id = sheet_id
        else:
            self.file_name = "../dataconfig/testcase.xls"
            self.sheet_id = 0
        self.data = self.get_data()

    def get_data(self):
        """
        获取sheets的内容
        :return: 整个工作表的内容
        """
        data = xlrd.open_workbook(self.file_name)
        tables = data.sheets()[self.sheet_id]
        return tables

    def get_lines(self):
        """ 获取单元格的行数 """
        tables = self.data
        return tables.nrows

    def get_cell_value(self, row, col):
        """ 获取某一个单元格的内容 """
        return self.data.cell_value(row, col)

    def write_value(self, row, col, value):
        """ 向指定单元格写入数据 """
        read_data = xlrd.open_workbook(self.file_name)
        write_data = copy(read_data)
        sheet_data = write_data.get_sheet(0)
        sheet_data.write(row, col, value)
        write_data.save(self.file_name)

    def get_rows_data(self, case_id):
        """ 根据对应的caseID找到对应的内容 """
        row_num = self.get_row_num(case_id)
        row_data = self.get_row_values(row_num)
        return row_data

    def get_row_num(self, case_id):
        """ 根据对应的caseID找到对应的行号 """
        num = 0
        cols_data = self.get_cols_data()
        for col_data in cols_data:
            if case_id in col_data:
                return num
            num = num+1

    def get_row_values(self, row):
        """ 根据行号，获取整行数据 """
        tables = self.data
        row_data = tables.row_values(row)
        return row_data

    def get_cols_data(self, col_id=None):
        """ 根据列号，获取某一列的内容 """
        if col_id:
            cols = self.data.col_values(col_id)
        else:
            cols = self.data.col_values(0)
        return cols