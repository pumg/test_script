#Coding:UTF-8
#Python3
import requests
import json


class RunMethod:

    def post_mian(self, url, data=None, header=None):
        """ post请求 """
        res = None
        if header !=None:
            res = requests.post(url=url, data=data, headers=header, verify=False)
        else:
            res = requests.post(url=url, data=data, verify=False)
        return res

    def get_mian(self, url, data, header=None):
        """ get请求 """
        res = None
        if header != None:
            res = requests.get(url=url, params=data, headers=header, verify=False)
        else:
            res = requests.get(url=url, params=data, verify=False)
        return res

    def sendImg(self, url, files, header):
        """ 上传图片 """
        res = requests.post(url=url, headers=header, files=files, verify=False)
        return res

    def get_is_post(self, method, url, data=None, header=None):
        """ 判断是get请求还是post请求 """
        res = None
        if method == "Post":
            res = self.post_mian(url=url, data=data, header=header)
        elif method == "Upload":
            res = self.sendImg(url=url, files=data, header=header)
        else:
            res = self.get_mian(url=url, data=data, header=header)
        return res

    def run_mian(self, method, url, data=None, header=None):
        """ 主函数 """
        res = None
        # 如果是json形似，就返回json格式
        try:
            res = self.get_is_post(method, url, data, header).json()
            return json.dumps(res, indent=2, sort_keys=True, ensure_ascii=False)
        # 否则返回text格式
        except:
            res = self.get_is_post(method, url, data, header).text
            return res


