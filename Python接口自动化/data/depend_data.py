from util.operatinon_excel import OperatinonExcel
from data.get_data import GetData
from jsonpath_rw import parse
from base.runmethod import RunMethod
from util.operation_json import OperationJson
import json


class DependdentData:

    def __init__(self, case_id):
        self.case_id = case_id
        self.opera_excel = OperatinonExcel()
        self.data = GetData()

    def get_data_line_data(self):
        """ 通过case_id去获取case_id的整行数据 """
        rows_data = self.opera_excel.get_rows_data(self.case_id)
        return rows_data

    def run_dependent(self):
        """ 执行依赖测试，获取结果 """
        run_method = RunMethod()
        op_json = OperationJson("../dataconfig/header.json")
        header = op_json.read_data()
        row_num = self.opera_excel.get_row_num(self.case_id)
        request_data = self.data.get_data_for_json(row_num)
        method = self.data.get_request_method(row_num)
        url = self.data.get_request_url(row_num)
        res = run_method.run_mian(method=method, url=url, data=request_data, header=header)
        return json.loads(res)

    def get_data_for_key(self, row, num):
        """ 根据依赖的key去获取执行依赖测试case的响应，然后返回 """
        depend_data = self.data.get_depend_key(row)
        dep = depend_data.split(",")
        response_data = self.run_dependent()
        json_exe = parse(dep[num])
        madle = json_exe.find(response_data)
        return [math.value for math in madle][0]