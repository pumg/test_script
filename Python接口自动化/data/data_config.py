
class global_var:
    """ 定义excel每一个字段的常量 """
    id = "0"
    request_name = "1"
    run = "2"
    url = "3"
    method = "4"
    header = "5"
    case_depend = "6"
    data_depend = "7"
    field_depend = "8"
    data = "9"
    expect = "10"
    result = "11"

def get_id():
    """ 获取caseID列 """
    return global_var.id

def get_name():
    """ 获取requset_name列 """
    return global_var.request_name

def get_run():
    """ 获取是否执行列 """
    return global_var.run

def get_url():
    """ 获取是接口路径列 """
    return global_var.url

def get_method():
    """ 获取请求类型列 """
    return global_var.method

def get_header():
    """ 获取请求头列 """
    return global_var.header

def get_case_depend():
    """ 获取依赖的case列 """
    return global_var.case_depend

def get_data_depend():
    """ 获取依赖的返回数据列 """
    return global_var.data_depend

def get_field_depend():
    """ 获取数据依赖字段列 """
    return global_var.field_depend

def get_data():
    """ 获取请求数据列 """
    return global_var.data

def get_expect():
    """ 获取预期结果列 """
    return global_var.expect

def get_result():
    """ 获取实际结果列 """
    return global_var.result


