from util.operatinon_excel import OperatinonExcel
from data import data_config
from util.operation_json import OperationJson


class GetData:

    def __init__(self):
        self.opera_excel = OperatinonExcel()

    def get_case_lines(self):
        """ 获取excel的行数，就是case个数 """
        return self.opera_excel.get_lines()

    def get_case_id(self, row):
        """ 获取用例ID名称 """
        col = int(data_config.get_id())
        id = self.opera_excel.get_cell_value(row, col)
        return id

    def get_case_name(self, row):
        """ 获取用例模块名称 """
        col = int(data_config.get_name())
        name = self.opera_excel.get_cell_value(row, col)
        return name

    def get_is_run(self, row):
        """ 获取是否执行 """
        flag = None
        col = int(data_config.get_run())
        run_model = self.opera_excel.get_cell_value(row, col)
        if run_model == "yes":
            flag = True
        else:
            flag = False
        return flag

    def is_header(self, row):
        """ 是否携带header """
        col = int(data_config.get_header())
        header = self.opera_excel.get_cell_value(row, col)
        if header != "":
            return header
        else:
            return None

    def get_request_method(self, row):
        """ 获取请求方式 """
        col = int(data_config.get_method())
        requset_method = self.opera_excel.get_cell_value(row, col)
        return requset_method

    def get_request_url(self, row):
        """ 获取URL """
        col = int(data_config.get_url())
        url = self.opera_excel.get_cell_value(row, col)
        return url

    def get_request_data(self, row):
        """ 获取请求数据 """
        col = int(data_config.get_data())
        data = self.opera_excel.get_cell_value(row, col)
        if data != "":
            return data
        return None

    def get_data_for_json(self, row):
        """ 通过获取关键字拿到data数据 """
        opera_json = OperationJson()
        request_data = opera_json.get_data(self.get_request_data(row))
        return request_data

    def get_expect_data(self, row):
        """ 获取预期结果 """
        col = int(data_config.get_expect())
        expect = self.opera_excel.get_cell_value(row, col)
        return expect

    def write_result(self, row, value):
        """ 写入实际结果 """
        col = int(data_config.get_result())
        self.opera_excel.write_value(row, col, value)

    def get_depend_key(self, row):
        """ 获取依赖数据的key """
        col = int(data_config.get_data_depend())
        depent_key = self.opera_excel.get_cell_value(row, col)
        if depent_key == "":
            return None
        else:
            return depent_key

    def is_depend(self, row):
        """ 判断是否有case依赖 """
        col = int(data_config.get_case_depend())
        depend_case_id = self.opera_excel.get_cell_value(row, col)
        if depend_case_id == "":
            return None
        else:
            return depend_case_id

    def get_depend_filed(self, row, num):
        """ 获取数据依赖字段 """
        col = int(data_config.get_field_depend())
        data = self.opera_excel.get_cell_value(row, col)
        if data == "":
            return None
        else:
            da = data.split(",")
            return da[num]


