from handle.register_handle import RegisterHandle
class RegisterBusiness(object):
    def __init__(self,driver):
        self.register_h = RegisterHandle(driver)
    # 进行总的操作封装
    def register_base(self,username,code,password,confirm_password,judge):
        self.register_h.click_go_register()
        self.register_h.send_register_username(username)
        self.register_h.send_register_code(code)
        self.register_h.send_register_password(password)
        self.register_h.send_register_confirm_password(confirm_password)
        self.register_h.click_register_button()
        self.register_h.get_error_text(judge)
    # 判断预期结果和实际结果进行返回
    def register_function(self,username,code,password,confirm_password,judge,asserText):
        self.register_base(username,code,password,confirm_password,judge)
        if self.register_h.get_error_text(judge) == asserText:
            print('提示信息正确！预期值与实际值一致:')
            print('预期值：' + asserText)
            print('实际值: ' + self.register_h.get_error_text(judge))
            return True
        else:
            print('提示信息错误！预期值与实际值不符：')
            print('预期值：' + asserText)
            print('实际值: ' + self.register_h.get_error_text(judge))
            return False


# driver = webdriver.Chrome()
# driver.get('http://192.168.1.88:8126/webhtml/dist/index.html#/login')
# driver.maximize_window()
# rh = RegisterBusiness(driver)
# rh.register_function('', '', '123456', '123456', 'hd', '手机号不能为空!')
