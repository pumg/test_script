#coding=utf-8
import time
from page.login_page import LoginPage
class RegisterHandle(object):
    def __init__(self,driver):
        self.driver = driver
        self.register_p = LoginPage(self.driver)
    # 点击去注册按钮
    def click_go_register(self):
        self.register_p.get_register_element().click()
    # 输入要注册的手机号码
    def send_register_username(self,user):
        return self.register_p.get_register_username_element().send_keys(user)
    # 点击获取验证码按钮
    def click_obtain_code(self):
        return self.register_p.get_register_obtain_code_element().click()
    # 判断alert窗口是否弹出
    def __call__(self, driver):
        try:
            alert = driver.switch_to.alert
            return alert
        except:
            return False
    # 获取验证码并输入验证码
    def send_register_code(self,code):
        if code=='code':
            self.click_obtain_code()
            time.sleep(2)
            if self.__call__(self.driver) == False:
                return print("没有获取到验证码!")
            else:
                al = self.driver.switch_to_alert()
                at = al.text
                cd = at.split(':')[1]
                time.sleep(1)
                al.accept()
                return self.register_p.get_register_code_element().send_keys(cd)
        else:
            return self.register_p.get_register_code_element().send_keys(code)
    # 输入注册密码
    def send_register_password(self,password):
        return self.register_p.get_register_password_element().send_keys(password)
    # 输入注册确认密码
    def send_register_confirm_password(self,confirm_password):
        return self.register_p.get_register_confirm_password_element().send_keys(confirm_password)
    # 点击注册账号按钮
    def click_register_button(self):
        return self.register_p.get_register_button_element().click()
    # 获取输错误的提示信息
    def get_error_text(self,judge):
        time.sleep(1)
        if judge == 'hd':
            text = self.register_p.get_error_text_element()
            if text == None:
                return print("没有获取到错误信息!")
            else:
                return text.text
        else:
            time.sleep(2)
            text = self.register_p.get_register_button_element().text
        return text
