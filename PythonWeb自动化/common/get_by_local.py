from util.read_ini import Readini
class GetByLocal:
    def __init__(self,driver):
        self.driver = driver
    def get_element(self,key,section=None):
        read_ini = Readini()
        local = read_ini.get_value(key,section)
        le = local.split('>')
        try:
            if len(le) == 2:
                by = local.split('>')[0]
                local_by = local.split('>')[1]
                if by == 'id':
                    return self.driver.find_element_by_id(local_by)
                elif by == 'class':
                    return self.driver.find_element_by_class_name(local_by)
                else:
                    return self.driver.find_element_by_xpath(local_by)
            else:
                by = local.split('>')[0]
                local_by = local.split('>')[1]
                local_index = local.split('>')[2]
                if by == 'id':
                    return self.driver.find_elements_by_id(local_by)[int(local_index)]
                elif by == 'class':
                    return self.driver.find_elements_by_class_name(local_by)[int(local_index)]
                else:
                    return self.driver.find_elements_by_xpath(local_by)[int(local_index)]
        except:
            return None






