# coding=utf-8
import HTMLTestRunner
import unittest
import time
# 添加要执行的测试用例文件
discover = unittest.defaultTestLoader.discover(start_dir=r"../case/",pattern='register_case.py')
# 获取时间戳
get_local = time.localtime()
get_time = time.strftime("%Y-%m-%d %H-%M-%S",get_local)
if __name__ == "__main__":
    # 定义报告存放路径和名称
    fp = open(r"F../report/first_case%s.html"%get_time,'wb')
    runner = HTMLTestRunner.HTMLTestRunner(stream=fp,
                            title='unittest+ddt+HTMLTestRunner自动化测试报告',
                            description='测试结果如下: ')
    runner.run(discover)
    fp.close()

















