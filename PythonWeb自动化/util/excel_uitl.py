import xlrd
class ExcelUtil:
    def __init__(self,excel_path=None,index=None):
        if excel_path == None:
            # Excel文件的路径
            excel_path = "../data/register_data.xls"
        if index == None:
            # 默认为0
            index = 0
        # 打开Excel方法
        self.data = xlrd.open_workbook(excel_path)
        # 获取到sheet的对象
        self.table = self.data.sheets()[index]
        # 获取Excel有多少行
        self.rows = self.table.nrows
    # 循环获取每行的数据
    def get_data(self):
        result = []
        for i in range(self.rows):
            col = self.table.row_values(i)
            result.append(col)
        return result
