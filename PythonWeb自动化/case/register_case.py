import os
import time
import sys
sys.path.append('F:/webUI')
from business.register_business import RegisterBusiness
from selenium import webdriver
import unittest
import ddt
# 导入读取参数化的文件
from util.excel_uitl import ExcelUtil
ex = ExcelUtil()
# 把读取到的Excel数据赋值给data
data = ex.get_data()
# 获取时间戳
get_local = time.localtime()
get_time = time.strftime("%Y-%m-%d %H-%M-%S",get_local)
@ddt.ddt
class FirstCase(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get('http://192.168.1.88:8126/webhtml/dist/index.html#/login')
        self.driver.maximize_window()
        self.login = RegisterBusiness(self.driver)
    @ddt.data(*data)
    def test_register_case(self,data):
        username, code, password, confirm_password, judge, asserText = data
        rf = self.login.register_function(username,code,password,confirm_password,judge,asserText)
        # 通过assert进行判断
        return self.assertTrue(rf,"测试失败")
    def tearDown(self):
        # 判断每次执行的用例有没有失败，失败的话就截图
        for method_name, error in self._outcome.errors:
            if error:
                case_name = self._testMethodName
                file_image_path = os.path.join("../report/" + case_name + get_time + ".png")
                self.driver.save_screenshot(file_image_path)
        self.driver.close()
if __name__ == '__main__':
    unittest.main()






a





